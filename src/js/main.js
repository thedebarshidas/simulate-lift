var liftCount = 0;
var floorCount = 0;
var contentHolder = document.getElementsByClassName("content");
var userRequests = [];

//clears the content holder div
function clearContent() {
  while (contentHolder[0].firstChild) {
    contentHolder[0].removeChild(contentHolder[0].firstChild);
  }

  //clear the userRequests array
  userRequests = [];
}

function addHR() {
  var hr = document.createElement("hr");
  contentHolder[0].appendChild(hr);
}

//set the lift count and floor count to 1
function setDefault() {
  liftCount = 1;
  floorCount = 1;
  document.getElementById("lift").value = 1;
  document.getElementById("floor").value = 1;
}

//validates the input data
function validateData() {
  //stores the input values OF lift and floor
  liftCount = document.getElementById("lift").value;
  floorCount = document.getElementById("floor").value;

  //input data validation and error handling
  if (liftCount > 0 && floorCount > 0) {
    liftCount = parseInt(liftCount);
    floorCount = parseInt(floorCount);

    if (liftCount > 100) {
      var userErrMsg =
        "Lift count cannot be more than 100. I mean seriously, how many lifts do you need?";
      alert(userErrMsg);
      setDefault();
    } else if (floorCount > 160) {
      var userErrMsg =
        "Floor count cannot be more than 160. Even the Burj Khalifa has only 160 floors.";
      alert(userErrMsg);
      setDefault();
    } else if (liftCount <= 0) {
      var userErrMsg =
        "Lift count cannot be less than 1. You need at least one lift to move around or Cheers to you using the stairs.";
      alert(userErrMsg);
      setDefault();
    } else if (floorCount <= 0) {
      var userErrMsg =
        "Floor count cannot be less than 1. Its a building, you need at least one floor. Duh!";
      alert(userErrMsg);
      setDefault();
    }
  } else {
    var userErrMsg = "Please enter a valid number.";
    alert(userErrMsg);
    setDefault();
  }
}

//generate the floors with floor numbers
function generateFloors() {
  for (var i = floorCount; i >= 0; i--) {
    var floor = document.createElement("div");
    var floorText = document.createElement("div");
    if (i == 0) {
      floorText.innerHTML = "G";
    } else {
      floorText.innerHTML = i;
    }
    floorText.className = "floorText";
    floor.appendChild(floorText);

    var floorButtonHolder = document.createElement("div");
    floorButtonHolder.className = "floorButtonHolder";

    if (i == floorCount) {
      //if the floor is the top floor, only generate the down button
      var downButton = document.createElement("button");
      downButton.innerHTML = "Down";
      downButton.className = "downButton";
      downButton.id = "down" + i;
      floorButtonHolder.appendChild(downButton);
    } else if (i == 0) {
      //if the floor is the ground floor, only generate the up button
      var upButton = document.createElement("button");
      upButton.innerHTML = "Up";
      upButton.className = "upButton";
      upButton.id = "up" + i;
      floorButtonHolder.appendChild(upButton);
    } else if (i > 0 && i < floorCount) {
      //if the floor is neither the top nor the ground floor, generate both up and down buttons
      var upButton = document.createElement("button");
      upButton.innerHTML = "Up";
      upButton.className = "upButton";
      upButton.id = "up" + i;
      floorButtonHolder.appendChild(upButton);

      var downButton = document.createElement("button");
      downButton.innerHTML = "Down";
      downButton.className = "downButton";
      downButton.id = "down" + i;
      floorButtonHolder.appendChild(downButton);
    }
    floor.appendChild(floorButtonHolder);

    floor.className = "floor";
    floor.id = "floor" + i;
    contentHolder[0].appendChild(floor);
  }
}

//generate the lifts
function generateLifts() {
  var liftHolder = document.createElement("div");
  liftHolder.className = "liftHolder";

  for (var i = 1; i <= liftCount; i++) {
    var lift = document.createElement("div");
    var liftLeftDoor = document.createElement("div");
    var liftRightDoor = document.createElement("div");
    liftLeftDoor.className = "liftLeftDoor";
    liftRightDoor.className = "liftRightDoor";
    lift.className = "lift";
    lift.classList.add("free");
    lift.classList.add("floor" + 0);
    lift.id = "lift" + i;
    lift.appendChild(liftLeftDoor);
    lift.appendChild(liftRightDoor);
    liftHolder.appendChild(lift);
  }

  contentHolder[0].appendChild(liftHolder);
}

//animate the lift doors opening and closing in 2.5s
function animateLiftDoors(liftId) {
  var freeLift = document.getElementById(liftId);
  var freeLiftLeftDoor = freeLift.childNodes[0];
  var freeLiftRightDoor = freeLift.childNodes[1];

  freeLiftLeftDoor.style.transform = "translateX(-1.6rem)";
  freeLiftLeftDoor.style.transitionDuration = "2.5s";
  freeLiftRightDoor.style.transform = "translateX(1.6rem)";
  freeLiftRightDoor.style.transitionDuration = "2.5s";

  setTimeout(() => {
    freeLiftLeftDoor.style.transform = "translateX(0rem)";
    freeLiftRightDoor.style.transform = "translateX(0rem)";
  }, 2500);

  setTimeout(() => {
    freeLift.classList.add("free");
    freeLift.classList.remove("busy");
  }, 5000);
}

//get the current floor of the lift
function getCurrentFloor(lift) {
  var currentFloor = lift.classList[2].substring(5);
  return currentFloor;
}

//animate the lift moving from one floor to another in 2.5s
//important to note that the lift movement is calculated in rem
function animateLiftMovement(liftId, liftMovement, actualMovement) {
  var lift = document.getElementById(liftId);
  var liftMovementInPx = liftMovement * 8 + liftMovement * 1 + "rem";
  console.log("Distance " + liftMovementInPx);

  lift.style.transform = "translateY(-" + liftMovementInPx + ")";

  //lift should take 2s to move from one floor to another
  lift.style.transitionDuration = actualMovement + "s";
  console.log("lift moved in " + lift.style.transitionDuration + " seconds");

  setTimeout(() => {
    animateLiftDoors(liftId);
  }, actualMovement * 1000);
}

//get the lift that is currently free
function getFreeLift() {
  var lifts = document.getElementsByClassName("lift");
  var freeLift = null;
  for (var i = 0; i < lifts.length; i++) {
    if (lifts[i].classList.contains("free")) {
      freeLift = lifts[i];
      break;
    } else {
      continue;
    }
  }
  return freeLift;
}

//handle requests from the user
function handleRequests() {
  if (userRequests.length == 0) {
    return;
  } else {
    //testpoint 0: View the requestQueue
    // console.log("Request Queue before finding free lift: " + userRequests);

    var freeLift = getFreeLift();

    //testpoint 1: display free lift id and floor
    // console.log("Free lift id: " + freeLift.id);
    // console.log("Free lift is at floor: " + getCurrentFloor(freeLift));

    //testpoint 1.1:  View the requestQueue
    // console.log("Request Queue after finding free lift: " + userRequests);

    if (freeLift != null) {
      var lift = freeLift;

      if (lift.classList.contains("floor" + userRequests[0])) {
        lift.classList.remove("free");
        lift.classList.add("busy");
        animateLiftDoors(lift.id);
        userRequests.shift();
      } else {
        var currentFloor = getCurrentFloor(lift);
        var actualMovement = Math.abs(currentFloor - userRequests[0]);
        actualMovement = actualMovement * 2;

        lift.classList.remove("floor" + getCurrentFloor(lift));
        lift.classList.remove("free");
        lift.classList.add("busy");
        lift.classList.add("floor" + userRequests[0]);

        //testpoint 2: display the floor of the lift it is moving to
        // console.log("Lift is moving to floor: " + lift.classList);
        // console.log("\n");
        animateLiftMovement(lift.id, userRequests.shift(), actualMovement);
      }
    } else if (freeLift == null) {
      console.log("No free lifts");
      console.log(
        "Request Queue when free lifts are not available: " + userRequests
      );
    }

    setTimeout(() => {
      handleRequests();
    }, 5000);
  }
}

//simulate the lifts moving from one floor to another and opening and closing the doors based on the user input and the lift's current floor when the user presses the up or down button
function simulate() {
  var upButtons = document.getElementsByClassName("upButton");
  var downButtons = document.getElementsByClassName("downButton");

  for (var i = 0; i < upButtons.length; i++) {
    upButtons[i].addEventListener("click", function () {
      var requestedFloor = this.id.substring(2);
      userRequests.push(requestedFloor);
      handleRequests();
    });
  }

  for (var i = 0; i < downButtons.length; i++) {
    downButtons[i].addEventListener("click", function () {
      var requestedFloor = this.id.substring(4);
      userRequests.push(requestedFloor);
      handleRequests();
    });
  }
}

//starts the simulation
function startSimulation() {
  clearContent();
  addHR();

  validateData();

  generateFloors();
  generateLifts();

  simulate();
}
